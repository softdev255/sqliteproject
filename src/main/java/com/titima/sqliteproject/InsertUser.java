/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.titima.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class InsertUser {

    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:user.db");
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            String sql = "INSERT INTO user (id,username,password) "
                    + "VALUES (1, 'Titima', 'mildti' );";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO user (id,username,password) "
                    + "VALUES (2, 'Chidchanook', 'modchid' );";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO user (id,username,password) "
                    + "VALUES (3, 'Kanyakorn', 'oumoim11' );";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO user (id,username,password) "
                    + "VALUES (4, 'Tanut', 'tonza55' );";
            stmt.executeUpdate(sql);
            conn.commit();
            stmt.close();
            conn.close();

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(InsertUser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(InsertUser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
